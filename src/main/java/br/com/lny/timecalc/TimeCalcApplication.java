package br.com.lny.timecalc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TimeCalcApplication {

	public static void main(String[] args) {
		SpringApplication.run(TimeCalcApplication.class, args);
	}

}
