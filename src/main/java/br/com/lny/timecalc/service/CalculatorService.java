package br.com.lny.timecalc.service;

import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

@Log4j2
@Service
public class CalculatorService {

    private final String formatDateTime = "yyyy-MM-ddTHH:mm:ss";
    private final String formatDate = "yyyy-MM-dd";
    private final String formatTime = "HH:mm:ss";

    /**
     * Calculate a date and time
     *
     * @param inputDateTime     {@link String} date and time base
     * @param days              number of days
     * @param months            number of months
     * @param years             number of years
     * @param hours             number of hours
     * @param minutes           number of minutes
     * @param seconds           number of seconds
     * @param operationDateType {@link OperationDateType} operation to add or less
     * @return a {@link String} with the result
     */
    public Mono<String> calculateDateTime(String inputDateTime,
                                          int days,
                                          int months,
                                          int years,
                                          int hours,
                                          int minutes,
                                          int seconds,
                                          OperationDateType operationDateType) {

        log.info("CalculatorService#calculateDateTime - request={} - operationType={}", "", operationDateType);

        return Mono.just(
                operationDateType.doCalcOperation(LocalDateTime.parse(inputDateTime),
                        days,
                        months,
                        years,
                        hours,
                        minutes,
                        seconds));
    }

    /**
     * Calculate a date
     *
     * @param inputDate         {@link String} date base
     * @param days              number of days
     * @param months            number of months
     * @param years             number of years
     * @param operationDateType {@link OperationDateType} operation to add or less
     * @return a {@link String} with the result
     */
    public Mono<String> calculateDate(String inputDate,
                                      int days,
                                      int months,
                                      int years,
                                      OperationDateType operationDateType) {
        return Mono.just(operationDateType.doCalcOperation(LocalDate.parse(inputDate, DateTimeFormatter.ofPattern(formatDate)).atStartOfDay(),
                days,
                months,
                years,
                0,
                0,
                0));
    }

    /**
     * Calculate a time
     *
     * @param startFrom         {@link String} time base using format [hours:minutes:seconds]
     * @param hours             number of hours
     * @param minutes           number of minutes
     * @param seconds           number of seconds
     * @param operationDateType {@link OperationDateType} operation to add or less
     * @return a {@link String} with the result
     */
    public Mono<String> calculate(String startFrom,
                                  int hours,
                                  int minutes,
                                  int seconds,
                                  OperationTimeType operationDateType) {
        Duration input = this.getDurationFromString(startFrom);
        return Mono.just(operationDateType.doCalcOperation(input,
                hours,
                minutes,
                seconds));
    }

    private Duration getDurationFromString(String startFrom) {
        try {
            Duration duration = Duration.between(LocalTime.MIN, LocalTime.parse(startFrom));
            log.debug(">>> getDurationFromString={}", duration);
            return duration;
        } catch (Exception e) {
            log.error(e.getMessage());
            throw e;
        }
    }

    /**
     * Calculate a date time using {@link OperationDateType}.
     *
     * @param inputDateTime     a {@link String} to calculate
     * @param days              the number of days
     * @param months            the number of months
     * @param years             the number of years
     * @param hours             the number of hours
     * @param minutes           the number of minutes
     * @param seconds           the number of seconds
     * @param operationDateType the operation {@link OperationDateType}
     * @param requestId         a {@link String} for the request
     * @return result of calculation
     */
    public Mono<String> calculate(String inputDateTime,
                                  int days,
                                  int months,
                                  int years,
                                  int hours,
                                  int minutes,
                                  int seconds,
                                  OperationDateType operationDateType,
                                  final String requestId) {
        log.info("CalculatorService#calculate - requestId={} inputDateTime={}", requestId, inputDateTime);
        return this.calculateDateTime(inputDateTime, days, months, years, hours, minutes, seconds, operationDateType);
    }
}




















