package br.com.lny.timecalc.service;

import lombok.extern.log4j.Log4j2;

import java.time.LocalDateTime;

/**
 * Type of operations
 */
@Log4j2
public enum OperationDateType {
    PLUS {
        @Override
        public String doCalcOperation(LocalDateTime input, int days, int months, int years, int hours, int minutes, int seconds) {
            return input.plusDays(days)
                    .plusMonths(months)
                    .plusYears(years)
                    .plusHours(hours)
                    .plusMinutes(minutes)
                    .plusSeconds(seconds)
                    .toString();
        }
    },
    MINUS {
        @Override
        public String doCalcOperation(LocalDateTime input, int days, int months, int years, int hours, int minutes, int seconds) {
            return input.minusDays(days)
                    .minusMonths(months)
                    .minusYears(years)
                    .minusHours(hours)
                    .minusMinutes(minutes)
                    .minusSeconds(seconds)
                    .toString();
        }
    };

    public abstract String doCalcOperation(LocalDateTime input, int days, int months, int years, int hours, int minutes, int seconds);
}
