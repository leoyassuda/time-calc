package br.com.lny.timecalc.service;

import lombok.extern.log4j.Log4j2;

import java.time.Duration;

/**
 * Type of operations
 */
@Log4j2
public enum OperationTimeType {
    PLUS {
        @Override
        public String doCalcOperation(Duration input, int hours, int minutes, int seconds) {
            Duration initialDuration = Duration.ZERO.plus(input);
            Duration accumulateDuration = initialDuration.plusHours(hours).plusMinutes(minutes).plusSeconds(seconds);
            log.info("Total duration was {} hours and {} minutes.",
                    accumulateDuration.toHours(),
                    accumulateDuration.toMinutesPart());
            return PLUS.formatter(accumulateDuration.toHours(),
                    accumulateDuration.toMinutesPart(),
                    accumulateDuration.toSecondsPart());
        }
    },
    MINUS {
        @Override
        public String doCalcOperation(Duration input, int hours, int minutes, int seconds) {
            Duration initialDuration = Duration.ofHours(0).plusMinutes(0).plusSeconds(0);
            Duration accumulateDuration = initialDuration.minusHours(hours).minusMinutes(minutes).minusSeconds(seconds);
            log.info("Total duration was {} hours and {} minutes.",
                    accumulateDuration.toHours(),
                    accumulateDuration.toMinutesPart());
            return MINUS.formatter(hours, minutes, seconds);
        }
    };

    public abstract String doCalcOperation(Duration input, int hours, int minutes, int seconds);

    private String formatter(long hours, long minutes, long seconds) {
        return String.format(
                "%02d:%02d:%02d",
                hours, minutes, seconds);
    }
}
