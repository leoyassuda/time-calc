package br.com.lny.timecalc.controller;

import br.com.lny.timecalc.service.CalculatorService;
import br.com.lny.timecalc.service.OperationTimeType;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Mono;

import javax.validation.constraints.NotNull;

@RestController
@RequestMapping("/api/time")
public class TimeController {

    @Autowired
    private CalculatorService calculatorService;

    @ApiResponses(value = {@ApiResponse(responseCode = "200", description = "calculate duration time")})
    @GetMapping("/")
    @ResponseStatus(HttpStatus.OK)
    public Mono<String> calculate(@NotNull @RequestParam(defaultValue = "0") String startFrom,
                             @NotNull @RequestParam("operation") OperationTimeType operation,
                             @RequestParam(defaultValue = "0") final int hours,
                             @RequestParam(defaultValue = "0") final int minutes,
                             @RequestParam(defaultValue = "0") final int seconds) {
        return calculatorService.calculate(startFrom, hours, minutes, seconds, operation);
    }
}
