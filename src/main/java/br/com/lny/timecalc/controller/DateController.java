package br.com.lny.timecalc.controller;

import br.com.lny.timecalc.service.CalculatorService;
import br.com.lny.timecalc.service.OperationDateType;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Mono;

import javax.validation.constraints.NotNull;
import java.util.UUID;

@RestController
@RequestMapping("/api/date")
public class DateController {

    @Autowired
    private CalculatorService calculatorService;

    @ApiResponses(value = {@ApiResponse(responseCode = "200", description = "calculate date and time using PLUS or MINUS operator")})
    @GetMapping("/")
    @ResponseStatus(HttpStatus.OK)
    public Mono<String> calculate(@NotNull @RequestParam("dateTime") String inputDateTime,
                             @NotNull @RequestParam("operation") OperationDateType operationDateType,
                             @RequestParam(defaultValue = "0") final int days,
                             @RequestParam(defaultValue = "0") final int months,
                             @RequestParam(defaultValue = "0") final int years,
                             @RequestParam(defaultValue = "0") final int hours,
                             @RequestParam(defaultValue = "0") final int minutes,
                             @RequestParam(defaultValue = "0") final int seconds) {
        final String requestId = UUID.randomUUID().toString();
        return calculatorService.calculate(inputDateTime,
                days,
                months,
                years,
                hours,
                minutes,
                seconds,
                operationDateType,
                requestId);
    }
}
