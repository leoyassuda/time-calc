package br.com.lny.timecalc.service;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class CalculatorServiceTest {

    private final CalculatorService calculatorService = new CalculatorService();

    @Test
    @DisplayName("Simple test using date and time calculator - plus operation")
    void when_calculateDateTimeOperationTypePlus_withDateAndTime_shouldReturnDateTimeCalculated() {
        String result = calculatorService.calculateDateTime("2020-07-31T00:00:00",
                1,
                1,
                10,
                1,
                10,
                30,
                OperationDateType.PLUS).block();
        assertEquals("2030-09-01T01:10:30", result);
    }

    @Test
    @DisplayName("Simple test using date calculator - plus operation")
    void when_calculateDateOperationTypePlus_withDate_shouldReturnDateTimeCalculated() {
        String result = calculatorService.calculateDate("2020-07-31",
                1,
                1,
                2,
                OperationDateType.PLUS).block();
        assertEquals("2022-09-01T00:00", result);
    }

    @Test
    @DisplayName("Simple test using time calculator - plus operation")
    void when_calculateTimeOperationTypePlus_withHourMinuteSecond_shouldReturnTimeCalculated() {
        String result = calculatorService.calculate("01:10:00",
                4,
                5,
                45,
                OperationTimeType.PLUS).block();
        assertEquals("05:15:45", result);
    }

}