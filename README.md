# Time Calculator

A simple project to calculate different types of time

## Getting Started

Cloning the repo
```
git clone https://gitlab.com/leoyassuda/time-calc.git
```

### Prerequisites

What things you need to install the software and how to install them

> - Java 11
> - Maven 3

### Installing

In the folder where repo was cloned, run the following commands

```
mvn clean install -DskipTests
```

## Running the tests

```
mvn test
```

### Running the application

```
mvn spring-boot:run
```

### API

> / = return only a 'OK' message
>
> /api/v1/test = GET, POST, PUT operations - (GET) return Images without relationships, only the basic properties
>

## Built With

* [Spring-boot](https://projects.spring.io/spring-boot/)
* [Maven](https://maven.apache.org/)

## Authors

* **Leo Yassuda** - *Initial work* - [Time-Calc](https://gitlab.com/leoyassuda/time-calc)
